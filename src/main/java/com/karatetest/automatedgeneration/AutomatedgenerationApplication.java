package com.karatetest.automatedgeneration;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AutomatedgenerationApplication {

	public static void main(String[] args) {
		SpringApplication.run(AutomatedgenerationApplication.class, args);
	}

}
